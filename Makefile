migrate_down:
	migrate -path internal/migrations -database postgres://postgres:0000@localhost:5432/auth?sslmode=disable down
migrate_up:
	migrate -path internal/migrations -database postgres://postgres:0000@localhost:5432/auth?sslmode=disable up
migrate_create:
	migrate create -ext sql -dir internal/migrations -seq unique_constraint