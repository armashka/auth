package errs

import "net/http"

var ErrUserExists = NewErr("User already registered", http.StatusConflict)
var ErrInvalidRequest = NewErr("Invalid request data", http.StatusBadRequest)
var ErrInternalServer = NewErr("Internal server error", http.StatusInternalServerError)
var ErrAccNotFound = NewErr("Account with given email not found", http.StatusBadRequest)
var ErrNotAuthorized = NewErr("Not authorized", http.StatusBadRequest)
var ErrTokenMalformed = NewErr("Token field malformed", http.StatusBadRequest)
var ErrTokenFieldNotSupported = NewErr("Token field not supported", http.StatusBadRequest)
var ErrTokenExpired = NewErr("Token expired", http.StatusBadRequest)

type Err struct {
	Err  string `json:"message"`
	Code int    `json:"-"`
}

func (e *Err) Error() string {
	return e.Err
}

func NewErr(message string, code int) error {
	return &Err{Err: message, Code: code}
}

type CommonError struct {
	ErrorMessage string `json:"error_message"`
}
