package middlewares

import (
	"context"
	"diploma/auth/internal/errs"
	"diploma/auth/internal/helper"
	"diploma/auth/internal/service"
	log "github.com/sirupsen/logrus"
	"net/http"
)

const TokenKey = "token-internal-jwt"

func AuthValidate(next http.HandlerFunc, tokenService service.JWTService, log *log.Logger) http.HandlerFunc {
	// chi realization
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		token := r.Header.Get("Authorization")
		if token == "" {
			helper.ResponseWithHttpError(r, w, log, errs.ErrNotAuthorized)
			return
		}

		_, err := tokenService.ValidateSimple(ctx, token)
		if err != nil {
			helper.ResponseWithHttpError(r, w, log, err)
			return
		}

		ctx = context.WithValue(ctx, TokenKey, token)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return fn
}

func GetToken(ctx context.Context) string {
	if ctx == nil {
		return ""
	}
	if token, ok := ctx.Value(TokenKey).(string); ok {
		return token
	}
	return ""
}
