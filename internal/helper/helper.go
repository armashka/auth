package helper

import (
	"context"
	"diploma/auth/internal/errs"
	"github.com/go-chi/chi/middleware"

	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func LoggerWithReqID(ctx context.Context, logger *log.Logger) *log.Entry {
	entry := logger.WithField("request_id", middleware.GetReqID(ctx))
	return entry
}

func ResponseWithHttpError(r *http.Request, w http.ResponseWriter, log *log.Logger, er error) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(determineErr(er))

	log.WithField("request_id", middleware.GetReqID(r.Context())).
		Errorf("unexpected error: %+v", er)

	err := json.NewEncoder(w).Encode(errs.CommonError{ErrorMessage: er.Error()})
	if err != nil {
		log.WithField("request_id", middleware.GetReqID(r.Context())).
			Errorf("could not encode error: %+v", err)
	}
}

func determineErr(err error) int {
	switch cErr := err.(type) {
	case *errs.Err:
		return cErr.Code
	default:
		err = errs.ErrInternalServer
		return http.StatusInternalServerError
	}
}

func ResponseWithHttp(r *http.Request, w http.ResponseWriter, msg interface{}, log *log.Logger, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	err := json.NewEncoder(w).Encode(msg)
	if err != nil {
		log.WithField("request_id", middleware.GetReqID(r.Context())).
			Errorf("could not encode response: %+v", msg)
	}
}
