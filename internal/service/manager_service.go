package service

import (
	"context"
	"diploma/auth/internal/errs"
	"diploma/auth/internal/helper"
	"diploma/auth/internal/model"
	"diploma/auth/internal/repository"
	log "github.com/sirupsen/logrus"
	"strconv"
)

type managerService struct {
	auth         AuthService
	jwt          JWTService
	cacheManager repository.Cache
	logger       *log.Logger
}

// TODO: refactor future login -> remove duplication

func (m *managerService) RefreshToken(ctx context.Context, token string) (*model.TokenPair, error) {

	ids, err := m.jwt.ValidateToken(ctx, token, []string{"id"})
	if err != nil {
		return nil, err
	}

	atoi, err := strconv.Atoi(ids[0])
	if err != nil {
		return nil, err
	}

	user, err := m.auth.GetById(ctx, atoi)
	if err != nil {
		return nil, err
	}

	pair, err := m.jwt.GenerateTokenPair(ctx, user)
	if err != nil {
		return nil, err
	}

	err = m.cacheManager.Insert(pair.Access, pair.Refresh)
	if err != nil {
		return nil, err
	}

	return pair, nil
}

func (m *managerService) Logout(ctx context.Context, signedToken string) error {
	exists, err := m.cacheManager.KeyExists(signedToken)
	if err != nil {
		helper.LoggerWithReqID(ctx, m.logger).Errorf("could not find token in cache {%s}", signedToken)
		return err
	}

	if !exists {
		helper.LoggerWithReqID(ctx, m.logger).Warningf("token not exists in cache {%s}", signedToken)
		return errs.ErrNotAuthorized
	}
	err = m.cacheManager.Delete(signedToken)
	if err != nil {
		helper.LoggerWithReqID(ctx, m.logger).Warningf("could not delete token in cache {%s}", signedToken)
		return err
	}

	return nil
}

func (m *managerService) Validate(ctx context.Context, jwt string, decryptFields []string) ([]string, error) {
	exists, err := m.cacheManager.KeyExists(jwt)
	if err != nil {
		return nil, err
	}

	if !exists {
		return nil, errs.ErrNotAuthorized
	}

	fields, err := m.jwt.ValidateToken(ctx, jwt, decryptFields)
	if err != nil {
		return nil, err
	}
	return fields, nil
}

func (m *managerService) Register(ctx context.Context, email, pass, lang string) (int, error) {
	userID, err := m.auth.Register(ctx, email, pass, lang)
	if err != nil {
		return 0, err
	}
	return userID, nil
}

func (m *managerService) Login(ctx context.Context, email, pass string) (*model.TokenPair, error) {
	user, err := m.auth.Login(ctx, email, pass)
	if err != nil {
		return nil, err
	}

	pair, err := m.jwt.GenerateTokenPair(ctx, user)
	if err != nil {
		return nil, err
	}

	err = m.cacheManager.Insert(pair.Access, pair.Refresh)
	if err != nil {
		return nil, err
	}

	return pair, nil
}

func NewManagerService(auth AuthService, jwt JWTService, cacheManager repository.Cache, logger *log.Logger) Manager {
	return &managerService{
		auth:         auth,
		jwt:          jwt,
		cacheManager: cacheManager,
		logger:       logger,
	}
}
