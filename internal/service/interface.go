package service

import (
	"context"
	"diploma/auth/internal/model"
	"github.com/dgrijalva/jwt-go"
)

type AuthService interface {
	Register(ctx context.Context, email, pass, lang string) (int, error)
	Login(ctx context.Context, email, pass string) (*model.User, error)
	GetById(ctx context.Context, userID int) (*model.User, error)
}

type JWTService interface {
	GenerateTokenPair(ctx context.Context, user *model.User) (*model.TokenPair, error)
	ValidateToken(ctx context.Context, tokenSigned string, decryptFields []string) ([]string, error)
	ValidateSimple(ctx context.Context, tokenSigned string) (jwt.MapClaims, error)
}

type Manager interface {
	Logout(ctx context.Context, signedToken string) error
	Validate(ctx context.Context, jwt string, decryptFields []string) ([]string, error)
	Register(ctx context.Context, email, pass, lang string) (int, error)
	Login(ctx context.Context, email, pass string) (*model.TokenPair, error)
	RefreshToken(ctx context.Context, token string) (*model.TokenPair, error)
}
