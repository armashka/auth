package service

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"diploma/auth/internal/errs"
	"diploma/auth/internal/helper"
	"diploma/auth/internal/model"
	"encoding/base64"
	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
	"io"
	"strconv"
	"time"
)

type jwtService struct {
	tokenSecret     string
	encryptSecret   string
	accessLifetime  time.Duration
	refreshLifetime time.Duration
	logger          *log.Logger
}

func NewJWTService(tokenSecret, encryptSecret string, accessLifetime, refreshLifetime time.Duration, logger *log.Logger) JWTService {
	return &jwtService{
		tokenSecret:     tokenSecret,
		encryptSecret:   encryptSecret,
		accessLifetime:  accessLifetime,
		refreshLifetime: refreshLifetime,
		logger:          logger,
	}
}

func (j *jwtService) encrypt(flied, key string) (string, error) {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}

	gcm, cipherErr := cipher.NewGCM(block)
	if cipherErr != nil {
		return "", cipherErr
	}

	nonce := make([]byte, gcm.NonceSize())

	if _, readErr := io.ReadFull(rand.Reader, nonce); readErr != nil {
		// internal server error
		return "", readErr
	}

	cipherTextByte := gcm.Seal(nonce, nonce, []byte(flied), nil)
	return base64.StdEncoding.EncodeToString(cipherTextByte), nil
}

func (j *jwtService) decrypt(field, key string) (string, error) {
	keyByte := []byte(key)
	block, blockErr := aes.NewCipher(keyByte)

	if blockErr != nil {
		return "", blockErr
	}

	gcm, cipherErr := cipher.NewGCM(block)
	if cipherErr != nil {
		return "", cipherErr
	}

	nonceSize := gcm.NonceSize()

	cipherTextByte, _ := base64.StdEncoding.DecodeString(field)
	nonce, cipherTextByteClean := cipherTextByte[:nonceSize], cipherTextByte[nonceSize:]

	decodedStringByte, gcmOpenErr := gcm.Open(nil, nonce, cipherTextByteClean, nil)

	if gcmOpenErr != nil {
		return "", gcmOpenErr
	}

	return string(decodedStringByte), nil
}

func (j *jwtService) GenerateTokenPair(ctx context.Context, user *model.User) (*model.TokenPair, error) {
	access, err := j.encryptFieldsAccess(user)
	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("access token encryption error for user {%v}", user)
		return nil, err
	}

	refresh, err := j.encryptFieldsRefresh(user)
	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("refresh token encryption error for user {%+v}", user)
		return nil, err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, access)
	accessString, err := token.SignedString([]byte(j.tokenSecret))
	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("access token signing error for user {%+v}", user)
		return nil, err
	}

	token = jwt.NewWithClaims(jwt.SigningMethodHS256, refresh)
	refreshString, err := token.SignedString([]byte(j.tokenSecret))
	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("refresh token signing error for user {%+v}", user)
		return nil, err
	}

	return &model.TokenPair{
		Access:  accessString,
		Refresh: refreshString,
	}, nil
}

func (j *jwtService) ValidateSimple(ctx context.Context, tokenSigned string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(
		tokenSigned, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				helper.LoggerWithReqID(ctx, j.logger).Warning("token method parse error")
				return nil, errs.ErrNotAuthorized
			}
			return []byte(j.tokenSecret), nil
		})

	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("token parse error")
		return nil, errs.ErrNotAuthorized
	}

	if !token.Valid {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("token is not valid")
		return nil, errs.ErrNotAuthorized
	}

	fields, err := j.extractTokenClaims(token)
	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("token claims parse error")
		return nil, err
	}

	mapClaims := fields.(jwt.MapClaims)
	parse, err := time.Parse(time.RFC3339, mapClaims["expiration"].(string))
	if err != nil {
		helper.LoggerWithReqID(ctx, j.logger).Errorf("token expiration parse error")
		return nil, errs.ErrNotAuthorized
	}

	if !time.Now().Before(parse) || err != nil {
		return nil, errs.ErrTokenExpired
	}

	return mapClaims, err
}

func (j *jwtService) extractTokenClaims(token *jwt.Token) (jwt.Claims, error) {
	if claims, ok := token.Claims.(jwt.Claims); ok {
		return claims, nil
	}
	return nil, errs.ErrNotAuthorized
}

func (j *jwtService) DecryptFields(ctx context.Context, toDecrypt []string, claims jwt.MapClaims) ([]string, error) {
	res := make([]string, len(toDecrypt))
	for k, v := range toDecrypt {
		cl, clOk := claims[v]
		if !clOk {
			helper.LoggerWithReqID(ctx, j.logger).Errorf("could not cound field {%s} in token", v)
			return nil, errs.ErrTokenFieldNotSupported
		}

		conv, convOk := cl.(string)
		if !convOk {
			helper.LoggerWithReqID(ctx, j.logger).Errorf("token field conversion error {%s}", v)
			return nil, errs.ErrTokenMalformed
		}

		decrField, decErr := j.decrypt(conv, j.encryptSecret)
		if decErr != nil {
			helper.LoggerWithReqID(ctx, j.logger).Errorf("token field decrypt error {%s}", v)
			return nil, errs.ErrTokenMalformed
		}
		res[k] = decrField
	}

	return res, nil
}

func (j *jwtService) encryptFieldsAccess(user *model.User) (jwt.MapClaims, error) {
	encryptedId, err := j.encrypt(strconv.Itoa(user.Id), j.encryptSecret)
	encryptedEmail, err := j.encrypt(user.Email, j.encryptSecret)
	encryptedLang, err := j.encrypt(user.Lang, j.encryptSecret)

	if err != nil {
		return nil, err
	}

	claims := jwt.MapClaims{
		"id":         encryptedId,
		"email":      encryptedEmail,
		"lang":       encryptedLang,
		"expiration": time.Now().Add(j.accessLifetime),
	}

	return claims, nil
}

func (j *jwtService) encryptFieldsRefresh(user *model.User) (jwt.MapClaims, error) {
	encryptedId, err := j.encrypt(strconv.Itoa(user.Id), j.encryptSecret)

	if err != nil {
		return nil, err
	}

	claims := jwt.MapClaims{
		"id":         encryptedId,
		"expiration": time.Now().Add(j.refreshLifetime),
	}

	return claims, nil
}

func (j *jwtService) ValidateToken(ctx context.Context, tokenSigned string, decryptFields []string) ([]string, error) {

	mapClaims, err := j.ValidateSimple(ctx, tokenSigned)
	if err != nil {
		return nil, err
	}

	decrypted, err := j.DecryptFields(ctx, decryptFields, mapClaims)
	if err != nil {
		return nil, err
	}

	return decrypted, nil
}
