package service

import (
	"context"
	"database/sql"
	"diploma/auth/internal/errs"
	"diploma/auth/internal/helper"
	"diploma/auth/internal/model"
	"diploma/auth/internal/repository"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type authService struct {
	repo   repository.AuthRepository
	logger *log.Logger
}

func (a *authService) GetById(ctx context.Context, userID int) (*model.User, error) {
	user, err := a.repo.GetUser(ctx, userID)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func New(authRep repository.AuthRepository, logger *log.Logger) AuthService {
	return &authService{
		repo:   authRep,
		logger: logger,
	}
}

func (a *authService) Register(ctx context.Context, email, pass, lang string) (int, error) {

	user, err := a.repo.GetByEmail(ctx, email)
	if err != nil {
		if err != sql.ErrNoRows {
			helper.LoggerWithReqID(ctx, a.logger).Errorf("could not find user by email {%s} and error {%s}", email, err.Error())
			return -1, err
		}
	}

	if user != nil {
		return -1, errs.ErrUserExists
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)

	if err != nil {
		helper.LoggerWithReqID(ctx, a.logger).Errorf("could not generate password hash for email {%s} ", email)
		return -1, err
	}

	userId, err := a.repo.Create(ctx, model.User{
		Email:    email,
		Password: string(hash),
		Lang:     lang,
	})

	if err != nil {
		helper.LoggerWithReqID(ctx, a.logger).Errorf("could not create user db for email {%s} ", email)
		return -1, err
	}

	//TODO: email and password verification
	//TODO: email notification for verification
	//TODO: email validation -> handler -> service (login check) -> rep (models) -> db (add migration for additional column)

	return userId, nil
}

func (a *authService) Login(ctx context.Context, email, pass string) (*model.User, error) {
	user, err := a.repo.GetByEmail(ctx, email)
	if err != nil {
		helper.LoggerWithReqID(ctx, a.logger).Warningf("could not find user by email {%s} ", email)
		return nil, errs.ErrAccNotFound
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(pass))
	if err != nil {
		helper.LoggerWithReqID(ctx, a.logger).Warningf("could not validate password for user by email {%s} ", email)
		return nil, errs.ErrNotAuthorized
	}

	return user, nil
}
