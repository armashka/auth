package model

type CredReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RefreshReq struct {
	Refresh string `json:"refresh"`
}

type Validates struct {
	DecryptFields []string `json:"decryptFields"`
}
