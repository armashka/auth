package repository

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
)

type repoRed struct {
	db *redis.Pool
	// 20 minutes -> 20 * 60
	cacheTime int
}

func NewRedis(db *redis.Pool, cacheTime int) Cache {
	return &repoRed{
		db:        db,
		cacheTime: cacheTime,
	}
}

func (r *repoRed) KeyExists(id string) (bool, error) {
	conn := r.db.Get()
	defer conn.Close()

	reply, err := redis.Int(conn.Do("EXISTS", id))

	if err != nil {
		fmt.Println(err.Error())
		return false, err
	}

	if reply == 1 {
		return true, nil

	} else {
		return false, nil
	}
}

func (r *repoRed) Insert(access string, refresh string) error {
	conn := r.db.Get()
	defer conn.Close()

	_, err := conn.Do("SET", access, refresh)
	if err != nil {
		return err
	}

	_, err = conn.Do("EXPIRE", access, r.cacheTime)

	if err != nil {
		return err
	}
	return nil
}

func (r *repoRed) Delete(access string) error {
	conn := r.db.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", access)

	if err != nil {
		return err
	}
	return nil
}
