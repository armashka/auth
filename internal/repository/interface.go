package repository

import (
	"context"
	"diploma/auth/internal/model"
)

type AuthRepository interface {
	Create(ctx context.Context, user model.User) (int, error)
	GetUser(ctx context.Context, id int) (*model.User, error)
	GetByEmail(ctx context.Context, email string) (*model.User, error)
	UpdateUser(ctx context.Context, user model.User) (int, error)
}

type Cache interface {
	KeyExists(id string) (bool, error)
	Delete(access string) error
	Insert(access string, refresh string) error
}
