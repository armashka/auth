package repository

import (
	"context"
	"diploma/auth/internal/model"
	"github.com/jmoiron/sqlx"
	"log"
)

type repo struct {
	db             *sqlx.DB
	stmtGetByEmail *sqlx.Stmt
	stmtGetByID    *sqlx.Stmt
	stmtCreate     *sqlx.Stmt
	stmtUpdate     *sqlx.Stmt
}

func (r *repo) GetUser(ctx context.Context, id int) (*model.User, error) {
	u := &model.User{}
	err := r.stmtGetByID.GetContext(ctx, u, id)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (r *repo) GetByEmail(ctx context.Context, email string) (*model.User, error) {
	u := &model.User{}
	err := r.stmtGetByEmail.GetContext(ctx, u, email)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (r *repo) UpdateUser(ctx context.Context, user model.User) (int, error) {
	var id int
	err := r.stmtUpdate.GetContext(ctx, id, user.Email, user.Password, user.Lang, user.Id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (r *repo) Create(ctx context.Context, user model.User) (int, error) {
	var id int
	err := r.stmtCreate.GetContext(ctx, &id, user.Email, user.Password, user.Lang)
	return id, err
}

func New(ctx context.Context, db *sqlx.DB) (AuthRepository, error) {
	stmtGetByID, err := db.PreparexContext(ctx,
		`SELECT * FROM auth.public.auth WHERE id = $1`)

	stmtGetByEmail, err := db.PreparexContext(ctx,
		`SELECT * FROM auth.public.auth WHERE email ilike $1`)

	stmtCreate, err := db.PreparexContext(ctx,
		`INSERT INTO auth.public.auth (email, password, lang, created_at, updated_at) values ($1, $2, $3, now(), now()) returning id`)

	stmtUpdate, err := db.PreparexContext(ctx,
		`UPDATE auth.public.auth SET email = $1, password = $2, lang = $3, updated_at = now() WHERE id = $4 returning id`)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	return &repo{
		db:             db,
		stmtGetByEmail: stmtGetByEmail,
		stmtGetByID:    stmtGetByID,
		stmtCreate:     stmtCreate,
		stmtUpdate:     stmtUpdate,
	}, nil
}
