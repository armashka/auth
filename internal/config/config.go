package config

import (
	"gopkg.in/yaml.v3"
	"os"
	"time"
)

type Config struct {
	Server Server `yaml:"server"`
	JWT    JWT    `yaml:"jwt"`
	DB     DB     `yaml:"db"`
	Redis  Redis  `yaml:"redis"`
}

type Redis struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type Server struct {
	Addr    string `yaml:"port"`
	Timeout struct {
		Server time.Duration `yaml:"server"`
		Write  time.Duration `yaml:"write"`
		Read   time.Duration `yaml:"read"`
		Idle   time.Duration `yaml:"idle"`
	} `yaml:"timeout"`
}

type JWT struct {
	TimeOut        time.Duration `yaml:"lifetime"`
	RefreshTimeOut time.Duration `yaml:"refresh_time"`
	Secret         string        `yaml:"secret"`
	Encrypt        string        `yaml:"secret_encrypt"`
}

type DB struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
	User string `yaml:"user"`
	Pass string `yaml:"password"`
	Name string `yaml:"name"`
}

func NewConfig(configPath string) (*Config, error) {
	// Create config structure
	config := &Config{}

	// Open config file
	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Init new YAML decode
	d := yaml.NewDecoder(file)

	// Start YAML decoding from file
	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}
