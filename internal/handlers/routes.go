package handlers

import (
	"diploma/auth/internal/middleware"
	"diploma/auth/internal/service"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	log "github.com/sirupsen/logrus"
)

func RegisterEndpoints(router *chi.Mux, manager service.Manager, jwt service.JWTService, logger *log.Logger) {
	h := newHandler(manager, logger)

	router.Route("/api/v1", func(r chi.Router) {

		r.Use(middleware.RequestID)
		r.Use(middleware.Logger)
		r.Use(middleware.Recoverer)

		r.Post("/login", h.Login)
		r.Post("/register", h.Register)
		r.Post("/logout", middlewares.AuthValidate(h.Logout, jwt, logger))

		r.Post("/refresh", h.RefreshToken)
		r.Post("/validate", h.ValidateTokenFields)
	})
}
