package handlers

import (
	"diploma/auth/internal/errs"
	"diploma/auth/internal/helper"
	customM "diploma/auth/internal/middleware"
	"diploma/auth/internal/model"
	"diploma/auth/internal/service"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type handler struct {
	manager service.Manager
	logger  *log.Logger
}

func newHandler(manager service.Manager, logger *log.Logger) handler {
	return handler{
		manager: manager,
		logger:  logger,
	}
}

func (h *handler) Register(w http.ResponseWriter, r *http.Request) {
	var req model.CredReq
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, errs.ErrInvalidRequest)
		return
	}

	id, err := h.manager.Register(r.Context(), req.Email, req.Password, r.Header.Get("lang"))
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, err)
		return
	}

	helper.ResponseWithHttp(r, w, struct{ Id int }{Id: id}, h.logger, http.StatusOK)
}

func (h *handler) Login(w http.ResponseWriter, r *http.Request) {
	var req model.CredReq
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, errs.ErrInvalidRequest)
		return
	}

	login, err := h.manager.Login(r.Context(), req.Email, req.Password)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, err)
		return
	}

	helper.ResponseWithHttp(r, w, login, h.logger, http.StatusOK)
}

func (h *handler) Logout(w http.ResponseWriter, r *http.Request) {
	err := h.manager.Logout(r.Context(), customM.GetToken(r.Context()))
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, err)
		return
	}

	helper.ResponseWithHttp(r, w, struct{ Message string }{Message: "success"}, h.logger, http.StatusOK)
}

func (h *handler) ValidateTokenFields(w http.ResponseWriter, r *http.Request) {

	var req model.Validates
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, errs.ErrInvalidRequest)
		return
	}

	tokens, err := h.manager.Validate(r.Context(), customM.GetToken(r.Context()), req.DecryptFields)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, err)
		return
	}

	helper.ResponseWithHttp(r, w, struct{ DecryptedFields []string }{DecryptedFields: tokens}, h.logger, http.StatusOK)
}

func (h *handler) RefreshToken(w http.ResponseWriter, r *http.Request) {
	var req model.RefreshReq
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, errs.ErrInvalidRequest)
		return
	}

	token, err := h.manager.RefreshToken(r.Context(), req.Refresh)
	if err != nil {
		helper.ResponseWithHttpError(r, w, h.logger, err)
		return
	}

	helper.ResponseWithHttp(r, w, token, h.logger, http.StatusOK)
}
