module diploma/auth

go 1.18

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.1
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/gomodule/redigo v1.8.8
	github.com/jmoiron/sqlx v1.3.5
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/lib/pq v1.10.0 // indirect
	go.uber.org/atomic v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20211013075003-97ac67df715c // indirect
)
