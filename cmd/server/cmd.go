package cmd

import (
	"context"
	_ "database/sql"
	"diploma/auth/internal/config"
	"diploma/auth/internal/handlers"
	"diploma/auth/internal/repository"
	"diploma/auth/internal/service"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/gomodule/redigo/redis"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func Run() {
	logger := log.New()
	logger.SetFormatter(&log.JSONFormatter{})

	// get configs
	conf, err := config.NewConfig("./config.yaml")
	if err != nil {
		logger.Fatalln(err)
	}

	dbUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		conf.DB.User, conf.DB.Pass, conf.DB.Host, conf.DB.Port, conf.DB.Name)

	logger.Info("creating connection to db...")

	db, err := sqlx.Open("postgres", dbUrl)
	if err != nil {
		logger.Fatalln("failed tp create connection to db", "error", err)
	}

	pool, err := connectToRedis(conf)
	if err != nil {
		logger.Fatalln("failed to connect redis", "error", err)
	}

	logger.Info("created connection to db successfully.")

	cxt, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	logger.Info("Init attempt repositories.")
	authRep, err := repository.New(cxt, db)
	if err != nil {
		logger.Fatalln("failed to init auth repository", "error", err)
	}

	cache := repository.NewRedis(pool, 1000) // seconds about 17 minutes

	logger.Info("Repositories successfully initialized.")

	logger.Info("Init attempt Services.")
	jwtService := service.NewJWTService(conf.JWT.Secret, conf.JWT.Encrypt, conf.JWT.TimeOut, conf.JWT.RefreshTimeOut, logger)
	authService := service.New(authRep, logger)
	manager := service.NewManagerService(authService, jwtService, cache, logger)
	logger.Info("Services successfully initialized.")

	logger.Info("Init handlers and routes.")
	r := router()
	handlers.RegisterEndpoints(r, manager, jwtService, logger)
	logger.Info("Handlers and Routes successfully initialized.")

	logger.Infof("Starting server on port {%s}.", conf.Server.Addr)
	server := &http.Server{
		Addr:           ":" + conf.Server.Addr,
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   2 * time.Minute,
		MaxHeaderBytes: 1 << 20,
	}

	errChan := make(chan error, 1)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		if err := server.ListenAndServe(); err != nil {
			errChan <- err
		}
	}()

	exitCode := 0

	select {
	case <-stop:
	case srvErr := <-errChan:
		exitCode = 1
		logger.Errorf("server stopped with error: {%s}", srvErr)
	}

	restApiShutdownCtx, restApiShutdownCtxCancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer restApiShutdownCtxCancel()

	err = server.Shutdown(restApiShutdownCtx)
	if err != nil {
		logger.Errorf("Fail to shutdown http-api with error: {%s}", err)
	}

	os.Exit(exitCode)
}

func connectToRedis(conf *config.Config) (*redis.Pool, error) {
	pool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp",
				conf.Redis.Host+":"+conf.Redis.Port,
				//redis.DialUsername(),
				//redis.DialPassword()
			)
			if err != nil {
				return conn, err
			}
			return conn, nil
		},
	}
	return pool, nil
}

func router() *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	router.Use(cors.Handler(cors.Options{
		AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))
	return router
}
